#ifndef __LED_FUNC_H__
#define __LED_FUNC_H__

#include <stdbool.h>
#include <stdint.h>

typedef enum
{
    LED_ID_1=0,
    LED_ID_2,
    LED_ID_3,
    LED_ID_4,
    MAXNUM_LED_IDs
} led_id_t;

typedef enum
{
    LED_COLOR_GREEN = 0,
    LED_COLOR_OFF,
    MAXNUM_LED_COLORs
} led_color_t;

typedef enum
{
    LED_STYLE_SOLID = 0,
    LED_STYLE_BLINK,
    MAXNUM_LED_STYLEs
} led_style_t;

typedef struct
{
    bool 	active;
    led_style_t action; 		// blink, solid
    led_color_t solid_color;	// set led color for solid mode
    led_color_t blink_color[2];	// set 2 led colors for blink mode - 0:fore, 1:back
    uint8_t blink_index; 	// on or off for blink mode - o:on, 1:off
    uint32_t blink_period;	// blinking (half) period
    uint32_t blink_tsamp;	// time stamp to control blink
    uint32_t action_timeout_ms;	// turn off led after specified time, 0 means not specified(infinite)
    uint32_t action_tstamp;	// time stamp to control turn off timer
} st_led_status_t;


void led_set_color(led_id_t idx, led_color_t color);
void set_led_action_blink(led_id_t idx, led_color_t color_fore, led_color_t color_back, uint32_t period, uint32_t timeout);
void set_led_action_solid(led_id_t idx, led_color_t color, uint32_t timeout);
void set_led_action_stop(led_id_t idx);
void task_led_player(void);

#endif
