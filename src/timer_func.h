#ifndef __TIMER_FUNC_H__
#define __TIMER_FUNC_H__

#include "app_timer.h"

APP_TIMER_DEF(m_slow_task_timer_id);
APP_TIMER_DEF(m_fast_task_timer_id);

#define SLOW_TASK_MS 100
#define FAST_TASK_MS 10

#define SLOW_TASK_INTERVAL APP_TIMER_TICKS(SLOW_TASK_MS)
#define FAST_TASK_INTERVAL APP_TIMER_TICKS(FAST_TASK_MS)

uint32_t interval(uint32_t past, uint32_t current);

void slow_task_timeout_handler(void *p_context);
void fast_task_timeout_handler(void *p_context);

void app_timers_init(void);
void app_timers_start(void);

#endif
