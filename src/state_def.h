#ifndef __STATE_DEF_H__
#define __STATE_DEF_H__

#include "state_event.h"
#include "state_func.h"

void init_state_table(void);
void state_init(void);
void states_update(event_t event);
void task_states(void);
void print_event(event_t event);
void print_state(state_phase_t st, state_t state);

#endif

