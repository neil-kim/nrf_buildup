#ifndef __STATE_EVENT_H__
#define __STATE_EVENT_H__

#include <stdint.h>
#include <stdbool.h>

typedef enum
{
	EVENT_BTN_POWER=0,
	EVENT_BTN_POWER_LONG,
	EVENT_BTN_MODE,
	EVENT_BTN_MODE_LONG,
	EVENT_BTN_LEVEL,
	EVENT_BTN_LEVEL_LONG,
	EVENT_BTN_BLE,
	EVENT_BTN_BLE_LONG,
	EVENT_STATE_TIMEOUT,
	MAXNUM_EVENTs,
	EVENT_STATE_POLL,
} event_t;

void event_flag_set(uint8_t event_id);
bool event_flag_get(uint8_t event_id);
void event_flag_clear(uint8_t event_id);
void event_flag_reset(void);
bool event_flag_is_empty(void);
event_t get_event(void);
void print_event(event_t event);

#endif

