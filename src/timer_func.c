#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "config_func.h"

#include "timer_func.h"
#include "led_func.h"
#include "io_func.h"
#include "state_def.h"

uint32_t interval(uint32_t past, uint32_t current)
{
    return (current - past);
}

void slow_task_timeout_handler(void *p_context)
{
    UNUSED_PARAMETER(p_context);
    task_led_player();
}

void fast_task_timeout_handler(void *p_context)
{
    UNUSED_PARAMETER(p_context);
    poll_button();
    task_states();
}

void app_timers_init(void)
{
    ret_code_t err_code;

    // Initialize timer module.
    err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);

    // Create timers.
    err_code = app_timer_create(&m_slow_task_timer_id, APP_TIMER_MODE_REPEATED, slow_task_timeout_handler);
    APP_ERROR_CHECK(err_code);

    err_code = app_timer_create(&m_fast_task_timer_id, APP_TIMER_MODE_REPEATED, fast_task_timeout_handler);
    APP_ERROR_CHECK(err_code);
}

void app_timers_start(void)
{
    ret_code_t err_code;

    // Start application timers.
    err_code = app_timer_start(m_slow_task_timer_id, SLOW_TASK_INTERVAL, NULL);
    APP_ERROR_CHECK(err_code);

    err_code = app_timer_start(m_fast_task_timer_id, FAST_TASK_INTERVAL, NULL);
    APP_ERROR_CHECK(err_code);
}
