#ifndef __DEFINE_H__
#define __DEFINE_H__

#include "SEGGER_RTT.h"

#define VER_MAJOR 0x00
#define VER_MINOR 0x00
#define VER_PATCH 0x09


#define RTT_PRINTF(...) \
do { \
 	char str[64];\
 	sprintf(str, __VA_ARGS__);\
 	SEGGER_RTT_WriteString(0, str);\
 } while(0)

#endif
