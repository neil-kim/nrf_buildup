#ifndef __IO_FUNC_H__
#define __IO_FUNC_H__

#include "boards.h"
#include "state_event.h"

#define POLL_PERIOD_MS 10
#define POLL_DEBOUNCE_MS 10
#define POLL_DEBOUNCE_CNT (POLL_DEBOUNCE_MS/POLL_PERIOD_MS)

#define DEBOUNCE_PRESS_TIME		5  // 50ms
#define LONG_PRESS_TIME			100 // 1000ms
#define VLONG_PRESS_TIME		200 // 2000ms


typedef enum
{
	POLARITY_ACTIVE_LOW=0,
	POLARITY_ACTIVE_HIGH,
	MAXNUM_POLARITYs,
} button_polarity_t;

typedef struct {
	button_polarity_t polarity;
	uint32_t pin;
	bool pressed;
	bool press_trigger_fired;
	uint32_t press_tick_count;
	event_t event_short;
	event_t event_long;
	event_t event_vlong;
} button_info_t;

void poll_button(void);
uint8_t get_mode(void);
uint8_t set_mode(uint8_t mode);
uint8_t set_next_mode(void);
uint8_t get_level(void);
uint8_t set_level(uint8_t level);
uint8_t set_next_level(void);

#endif
