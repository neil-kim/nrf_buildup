#include <stdint.h>
#include <string.h>

#include "config_func.h"

#include "define.h"
#include "state_event.h"
#include "io_func.h"
#include "led_func.h"
#include "state_def.h"
#include "state_func.h"

extern state_functions_t state_table[MAXNUM_STATEs];
static state_t current_state;

// -------------------------------------------------------------
state_t state_ent_on(event_t event)
{
	event_flag_reset();

	set_mode(0);
	set_level(0);
	set_led_action_blink(LED_ID_1, LED_COLOR_GREEN, LED_COLOR_OFF, 1000, 0);
	set_led_action_blink(LED_ID_2, LED_COLOR_GREEN, LED_COLOR_OFF, 1000, 0);

	return NO_STATE_CHANGE;
}

state_t state_act_on(event_t event)
{
	uint8_t val;
	state_t ret;
	ret = NO_STATE_CHANGE;

	switch (event)
	{
	case EVENT_BTN_POWER:
		break;
	case EVENT_BTN_POWER_LONG:
		ret = STATE_OFF;
		break;
	case EVENT_BTN_MODE:
		break;
	case EVENT_BTN_MODE_LONG:
		val = set_next_mode();
		if (val == 0)
			set_led_action_blink(LED_ID_1, LED_COLOR_GREEN, LED_COLOR_OFF, 1000, 0);
		else
			set_led_action_blink(LED_ID_1, LED_COLOR_GREEN, LED_COLOR_OFF, 100, 0);
		break;
	case EVENT_BTN_LEVEL:
		val = set_next_level();
		if (val == 0)
			set_led_action_blink(LED_ID_2, LED_COLOR_GREEN, LED_COLOR_OFF, 1000, 0);
		else if (val == 1)
			set_led_action_blink(LED_ID_2, LED_COLOR_GREEN, LED_COLOR_OFF, 500, 0);
		else
			set_led_action_blink(LED_ID_2, LED_COLOR_GREEN, LED_COLOR_OFF, 100, 0);
		break;
	case EVENT_BTN_LEVEL_LONG:
		break;
	case EVENT_BTN_BLE:
		break;
	case EVENT_BTN_BLE_LONG:
		break;
	case EVENT_STATE_TIMEOUT:
		ret = STATE_OFF;
		break;
	default:
		break;
	}

	return ret;
}

void state_ext_on(event_t event)
{
}

// -------------------------------------------------------------
state_t state_ent_off(event_t event)
{
	event_flag_reset();

	// todo:
	// change mode
	// change level
	// set led2 action
	// set led3 duty

	set_led_action_stop(LED_ID_1);
	set_led_action_stop(LED_ID_2);

	return NO_STATE_CHANGE;
}

state_t state_act_off(event_t event)
{
	state_t ret;
	ret = NO_STATE_CHANGE;

	switch (event)
	{
	case EVENT_BTN_POWER:
		break;
	case EVENT_BTN_POWER_LONG:
		ret = STATE_ON;
		break;
	case EVENT_BTN_MODE:
		break;
	case EVENT_BTN_MODE_LONG:
		break;
	case EVENT_BTN_LEVEL:
		break;
	case EVENT_BTN_LEVEL_LONG:
		break;
	case EVENT_BTN_BLE:
		break;
	case EVENT_BTN_BLE_LONG:
		break;
	case EVENT_STATE_TIMEOUT:
		break;
	default:
		break;
	}

	return ret;
}

void state_ext_off(event_t event)
{
}

// -------------------------------------------------------------
state_t get_state(void)
{
	return current_state;
}
void set_state(state_t state)
{
	current_state = state;
}
