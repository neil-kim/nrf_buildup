#include <stdbool.h>
#include <stdint.h>

#include "boards.h"

#include "define.h"
#include "config_func.h"
#include "io_func.h"
#include "state_event.h"


static button_info_t buttons[BUTTONS_NUMBER] =
{
	{
		.polarity = POLARITY_ACTIVE_LOW,
		.pin = BUTTON_1, // m_board_btn_list[0]
		.event_short = EVENT_BTN_POWER,
		.event_long = EVENT_BTN_POWER_LONG,
	},
	{
		.polarity = POLARITY_ACTIVE_LOW,
		.pin = BUTTON_2, // m_board_btn_list[1]
		.event_short = EVENT_BTN_MODE,
		.event_long = EVENT_BTN_MODE_LONG,
	},
	{
		.polarity = POLARITY_ACTIVE_LOW,
		.pin = BUTTON_3, // m_board_btn_list[2]
		.event_short = EVENT_BTN_LEVEL,
		.event_long = EVENT_BTN_LEVEL_LONG,
	},
	{
		.polarity = POLARITY_ACTIVE_LOW,
		.pin = BUTTON_4, // m_board_btn_list[3]
		.event_short = EVENT_BTN_BLE,
		.event_long = EVENT_BTN_BLE_LONG,
	},
};

void poll_button(void)
{
	int i;
	static uint16_t overlab_count = 0;

	// update physical button status
	for (int i = 0; i < BUTTONS_NUMBER; i++)
	{
		bool high = nrf_gpio_pin_read(buttons[i].pin) ? true : false;

		if (high == buttons[i].polarity)
		{
			// pressed
			if (!buttons[i].pressed)
			{
				buttons[i].press_tick_count = 0;
				buttons[i].pressed = true;
			}
		}
		else
		{
			// released
			if (buttons[i].pressed)
			{
				// release event
				buttons[i].pressed = false;
				if ((DEBOUNCE_PRESS_TIME < buttons[i].press_tick_count) && (buttons[i].press_tick_count < LONG_PRESS_TIME))
				{
					// short event - released in short time
					event_flag_set(buttons[i].event_short);
					NRF_LOG_INFO("button %d short", i);
				}
			}
		}
	}

	// check logical button status
	for (i = 0; i < BUTTONS_NUMBER; i++)
	{
		// Tick up only if we haven't already hit long press time, preventing
		// multiple fires of the hold callback
		if (buttons[i].pressed)
		{
			buttons[i].press_tick_count++;

			if (buttons[i].press_tick_count == LONG_PRESS_TIME)
			{
				// long press event
				event_flag_set(buttons[i].event_long);
				NRF_LOG_INFO("button %d long", i);
			}
#if 0
			// very long
			else if(buttons[i].press_tick_count == VLONG_PRESS_TIME)
			{
				// long press event
				event_flag_set(buttons[i].event_vlong);
			}
			// repeat
			else if( (buttons[i].press_tick_count > LONG_PRESS_TIME) && (buttons[i].press_tick_count % LONG_REPEAT_TIME == 0) )
			{
				// repeat event
				event_flag_set(buttons[i].event_repeat);
			}
#endif
		}
	}
}

static uint8_t m_mode=0;  // 0, 1
static uint8_t m_level=0; // 0, 1, 2

uint8_t get_mode(void)
{
        NRF_LOG_INFO("current mode: %d", m_mode);
	return m_mode;
}
uint8_t set_mode(uint8_t mode)
{
	if(mode>1) mode=1;
	m_mode = mode;
        NRF_LOG_INFO("set mode: %d", m_mode);
	return get_mode();
}
uint8_t set_next_mode(void)
{
	uint8_t mode;

	mode = (get_mode()+1) % 2; // 0, 1
	set_mode(mode);
	return get_mode();
}

uint8_t get_level(void)
{
        NRF_LOG_INFO("current level: %d", m_level);
	return m_level;
}
uint8_t set_level(uint8_t level)
{
	if(level>2) level=2;
	m_level = level;
        NRF_LOG_INFO("set level: %d", m_level);
	return get_level();
}
uint8_t set_next_level(void)
{
	uint8_t level;
	level = (get_level()+1) % 3; // 0, 1, 2
	set_level(level);
	return get_level();
}

