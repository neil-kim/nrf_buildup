#include <stdbool.h>
#include <stdint.h>

#include "boards.h"

#include "define.h"
#include "led_func.h"
#include "timer_func.h"


static st_led_status_t led[MAXNUM_LED_IDs];
static uint32_t led_tick;

void led_set_color(led_id_t idx, led_color_t color)
{
    switch(color)
    {
        case LED_COLOR_GREEN:
            bsp_board_led_on(idx);
            break;
        default:
        case LED_COLOR_OFF:
            bsp_board_led_off(idx);
        break;
    }
}

void set_led_action_blink(led_id_t idx, led_color_t color_fore, led_color_t color_back, uint32_t period, uint32_t timeout)
{
    int i;

    if(	(idx>=MAXNUM_LED_IDs) || (color_fore >= MAXNUM_LED_COLORs || color_back >= MAXNUM_LED_COLORs) )
        return;

    led_set_color(idx, color_fore);

    led[idx].active = true;
    led[idx].action = LED_STYLE_BLINK;
    led[idx].blink_color[0] = color_fore;
    led[idx].blink_color[1] = color_back;
    led[idx].blink_period = period;
    led[idx].action_timeout_ms = timeout;
    led[idx].action_tstamp = led_tick;

    for(i=0;i<MAXNUM_LED_IDs;i++)
    {
        led[i].blink_tsamp = led_tick;
        led[i].blink_index = 0;
    }
}

void set_led_action_solid(led_id_t idx, led_color_t color, uint32_t timeout)
{
    if(	(idx>=MAXNUM_LED_IDs) || (color >= MAXNUM_LED_COLORs ) )
        return;

    led_set_color(idx, color);

    led[idx].active = true;
    led[idx].action = LED_STYLE_SOLID;
    led[idx].solid_color = color;
    led[idx].action_timeout_ms = timeout;
    led[idx].action_tstamp = led_tick;
}

void set_led_action_stop(led_id_t idx)
{
    if(idx>=MAXNUM_LED_IDs)
        return;

    led_set_color(idx, LED_COLOR_OFF);

    led[idx].active = false;
    led[idx].action_timeout_ms = 0;
    led[idx].action_tstamp = led_tick;
}

void task_led_player(void)
{
    led_tick += SLOW_TASK_MS;

    for(int idx=0; idx<MAXNUM_LED_IDs; idx++)
    {
        if(!led[idx].active) continue;

        //blink
        if(led[idx].action == LED_STYLE_BLINK)
        {
            if( interval(led[idx].blink_tsamp, led_tick) >= led[idx].blink_period )
            {
                led[idx].blink_tsamp = led_tick;
                led[idx].blink_index ^= 1;

                led_set_color((led_id_t)idx, led[idx].blink_color[ led[idx].blink_index ] );
            }
        }

        // solid on / off
        if(led[idx].action == LED_STYLE_SOLID)
        {
            led_set_color((led_id_t)idx, led[idx].solid_color);
        }

        // led off after timeout - both of solid and blink
        if(led[idx].active && led[idx].action_timeout_ms!=0)
        {
            if( interval(led[idx].blink_tsamp, led_tick) >= led[idx].action_timeout_ms )
            {
                led[idx].active = false;
                led[idx].solid_color = LED_COLOR_OFF;
                led[idx].action_timeout_ms = 0;

                led_set_color((led_id_t)idx, LED_COLOR_OFF);
            }
        }
    }
}

