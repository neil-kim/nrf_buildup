#ifndef __STATE_FUNC_H__
#define __STATE_FUNC_H__

#include "state_event.h"

typedef enum
{
	STATE_OFF = 0,
	STATE_ON,
	MAXNUM_STATEs,
	NO_STATE_CHANGE
} state_t;

typedef enum
{
	STATE_PHASE_ENTER = 0,
	STATE_PHASE_ACTION,
	STATE_PHASE_EXIT,
	MAXNUM_STATE_FUNCs
} state_phase_t;

typedef state_t (*state_ent_func_t)(event_t);
typedef state_t (*state_act_func_t)(event_t);
typedef void (*state_ext_func_t)(event_t);

typedef struct
{
	state_ent_func_t entry_function;
	state_act_func_t action_function;
	state_ext_func_t exit_function;
} state_functions_t;

state_t state_ent_on(event_t event);
state_t state_act_on(event_t event);
void state_ext_on(event_t event);

state_t state_ent_off(event_t event);
state_t state_act_off(event_t event);
void state_ext_off(event_t event);

state_t get_state(void);
void set_state(state_t state);

#endif
