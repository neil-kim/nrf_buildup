#include <stdio.h>

#include "config_func.h"

#include "define.h"
#include "state_def.h"
#include "state_event.h"

#if 0
// compile error @ KEIL compiler
const state_functions_t state_table[MAXNUM_STATEs] =
{
	{	// STATE_ON,
		.entry_function 	= state_ent_on,
		.action_function 	= state_act_on,
		.exit_function 		= state_ext_on
	},
	{	// STATE_OFF,
		.entry_function 	= state_ent_off,
		.action_function 	= state_act_off,
		.exit_function 		= state_ext_off
	},
};

#else
state_functions_t state_table[MAXNUM_STATEs];

//caution: do NEVER re-assign
void init_state_table(void)
{
	state_table[STATE_ON].entry_function = state_ent_on;
	state_table[STATE_ON].action_function = state_act_on;
	state_table[STATE_ON].exit_function = state_ext_on;

	state_table[STATE_OFF].entry_function = state_ent_off;
	state_table[STATE_OFF].action_function = state_act_off;
	state_table[STATE_OFF].exit_function = state_ext_off;
}
#endif

// -------------------------------------------------------------
void state_init()
{
	init_state_table();
	set_state(STATE_OFF);
	state_ent_off(EVENT_STATE_POLL);
}

void states_update(event_t event)
{
	state_t new_state;

	if (state_table[get_state()].action_function != NULL)
	{
		new_state = state_table[get_state()].action_function(event);
	}
	else
	{
		// never come here
		state_init();
		return;
	}

	while (new_state != NO_STATE_CHANGE)
	{
		if (state_table[get_state()].exit_function != NULL)
		{
			print_state(STATE_PHASE_EXIT, get_state());
			state_table[get_state()].exit_function(event);
		}

		set_state(new_state);
		new_state = NO_STATE_CHANGE;
		if (state_table[get_state()].entry_function != NULL)
		{
			print_state(STATE_PHASE_ENTER, get_state());
			new_state = state_table[get_state()].entry_function(event);
		}
	}
}

void task_states(void)
{
	event_t event;
	event = get_event();

	if (event != EVENT_STATE_POLL)
	{
		print_event(event);
	}

	states_update(event);
}

void print_state(state_phase_t st, state_t state)
{
	char buff[64];
	int len;

	switch (st)
	{
	case STATE_PHASE_ENTER:
		len = sprintf(buff, "[STATE] ENTER ");
		break;
	case STATE_PHASE_ACTION:
		len = sprintf(buff, "[STATE] ACTION ");
		break;
	case STATE_PHASE_EXIT:
		len = sprintf(buff, "[STATE] EXIT ");
		break;
	default:
		len = sprintf(buff, "[STATE] UNDEFINED ");
		break;
	}

	switch (state)
	{
	case STATE_ON:
		sprintf(buff+len, "STATE_ON");
		break;
	case STATE_OFF:
		sprintf(buff+len, "STATE_OFF");
		break;
	default:
		sprintf(buff+len, "UNDEFINED");
		break;
	}

	RTT_PRINTF("%s\n", buff);
	NRF_LOG_INFO("%s", NRF_LOG_PUSH(buff));
}
